package com.muraliveeravalli.class0_coding_warm_up;

public class PrintStarTriangle {

    public static void main(String[] args) {
        printStarTriangle(5);

    }

    static void printStarTriangle(int rows) {

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }
}
