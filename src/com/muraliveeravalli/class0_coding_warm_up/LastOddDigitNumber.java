package com.muraliveeravalli.class0_coding_warm_up;

public class LastOddDigitNumber {

    public static void main(String[] args) {
        int arr[] = {1243, 311, 211, 56};
        System.out.println(lastOddDigitNumber(arr));
        int arr1[] = {21, 12, 2, 56};
        System.out.println(lastOddDigitNumber(arr1));
        int arr2[] = {21, 12};
        System.out.println(lastOddDigitNumber(arr2));

    }

    static int lastOddDigitNumber(int a[]) {
        for (int i = a.length - 1; i >= 0; i--) {
            int count = 0;
            int n = a[i];
            while (n > 0) {
                n /= 10;
                count++;
            }
            if (count % 2 != 0) {
                return a[i];
            }
        }
        return -1;
    }
}
