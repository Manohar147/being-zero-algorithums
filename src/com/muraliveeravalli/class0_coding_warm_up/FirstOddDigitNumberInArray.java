package com.muraliveeravalli.class0_coding_warm_up;

public class FirstOddDigitNumberInArray {

    public static void main(String[] args) {
        int arr[] = {1243, 311, 211, 56};
        System.out.println(firstOddDigitNumber(arr));
        int arr1[] = {21, 12, 2, 56};
        System.out.println(firstOddDigitNumber(arr1));
        int arr2[] = {21, 12};
        System.out.println(firstOddDigitNumber(arr2));

    }

    static int firstOddDigitNumber(int a[]) {
        for (int value : a) {

            int count = 0;
            int n = value;
            while (n > 0) {
                n /= 10;
                count++;
            }
            if (count % 2 != 0) {
                return value;
            }
        }
        return -1;
    }
}
