package com.muraliveeravalli.class0_coding_warm_up;

public class RelationalOperator {


    public static void main(String[] args) {
        printRelation(3, 2);
        System.out.println(printRelation(2, 3));
        System.out.println(printRelation(3, 2));
        System.out.println(printRelation(2, 2));
        System.out.println(printRelation(-3, 2));
        System.out.println(printRelation(2, -3));
    }

    static String printRelation(int a, int b) {
        return a + (a == b ? " = " : a < b ? " < " : " > ") + b;
    }
}
