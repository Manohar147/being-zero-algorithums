package com.muraliveeravalli.class0_coding_warm_up;

public class SeniorCitizen {

    public static void main(String[] args) {
        System.out.println(getSeniorCitizenship('m', 60));
        System.out.println(getSeniorCitizenship('m', 29));
        System.out.println(getSeniorCitizenship('f', 59));
    }

    static String getSeniorCitizenship(char g, int age) {
        return ((g == 'm' && age < 60) || (g == 'f' && age < 58)) ? "Not A Senior Citizen" : "Senior Citizen";
    }

}
