package com.muraliveeravalli.class0_coding_warm_up;

public class IsPrime {

    public static void main(String[] args) {
        System.out.println(isPrime(13));
        System.out.println(isPrime(1024));
        System.out.println(isPrime(198527117));

    }

    static boolean isPrime(int n) {
        if (n == 1) {
            return false;
        }
        for (int i = 2; i < Math.sqrt(n); i++) {
            System.out.print(".");
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
