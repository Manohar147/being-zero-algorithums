package com.muraliveeravalli.class0_coding_warm_up;

public class PrintCountColRectangle {

    public static void main(String[] args) {
        printCountColRectangle(3, 2);
        printCountColRectangle(3, 4);
    }

    static void printCountColRectangle(int rows, int cols) {
        int count = 0;
        for (int i = 0; i < rows; i++) {
            count++;
            for (int j = 0; j < cols; j++) {
                System.out.print(count + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }
}
