package com.muraliveeravalli.class0_coding_warm_up;

public class PrintAlternateSignWithoutMultiplesOfFour {

    public static void main(String[] args) {
        printAlternateSignWithoutMultiplesOfFour(10);
    }

    static void printAlternateSignWithoutMultiplesOfFour(int n) {
        int count = 0;
        int i = 1;

        while (count < n) {
            if (i % 4 != 0) {
                int sum = (int) (i * Math.pow(-1, count));
                System.out.print(sum + " ");
                count++;
            }
            i++;
        }
    }
}
