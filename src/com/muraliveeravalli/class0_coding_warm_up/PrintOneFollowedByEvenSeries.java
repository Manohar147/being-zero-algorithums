package com.muraliveeravalli.class0_coding_warm_up;

public class PrintOneFollowedByEvenSeries {

    public static void main(String[] args) {
        printOneFollowedByEvenSeries(5);
        printOneFollowedByEvenSeries(3);
    }

    static void printOneFollowedByEvenSeries(int n) {
        System.out.print("1 ");
        int count = 2, i = 4;
        while (count <= n) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                count++;
            }
            i++;
        }
    }
}
