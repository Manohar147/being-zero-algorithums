package com.muraliveeravalli.class6_linkedlist_foundation;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class ReturnPreviousNode {

    public static void main(String[] args) {
        LLNode head = insertAtEnd(null, 1);
        head = insertAtEnd(head, new int[]{2, 3, 4, 5});
        LLNode.printList(head);
        System.out.println(previousNode(head, 7));

    }

    static LLNode previousNode(LLNode head, int data) {
        if (head == null) {
            return null;
        }
        if (head.next == null) {
            return null;
        }
        if (head.next.data == data) {
            return head;
        }
        return previousNode(head.next, data);
    }
}
