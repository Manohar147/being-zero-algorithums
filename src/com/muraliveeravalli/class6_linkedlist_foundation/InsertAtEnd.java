package com.muraliveeravalli.class6_linkedlist_foundation;

public class InsertAtEnd {

    public static void main(String[] args) {
        LLNode head = insertAtEnd(null, 1);
        head = insertAtEnd(head, new int[]{2, 3, 4, 5});
        LLNode.printList(head);

    }

    public static LLNode insertAtEnd(LLNode head, int... data) {
        for (int i = 0; i < data.length; i++) {
            head = insertAtEnd(head, data[i]);
        }
        return head;
    }

    public  static LLNode insertAtEnd(LLNode head, int data) {
        if (head == null) {
            return new LLNode(data);
        }
        head.next = insertAtEnd(head.next, data);
        return head;
    }
}
