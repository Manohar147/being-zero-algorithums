package com.muraliveeravalli.class6_linkedlist_foundation;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class DeleteNode {

    public static void main(String[] args) {
        LLNode head = insertAtEnd(null, 1);
        head = insertAtEnd(head, new int[]{2, 3, 4, 5});
        LLNode.printList(head);
        head = deleteNode(head, 1);
        LLNode.printList(head);

    }

    static LLNode deleteNode(LLNode h, int data) {
        if (h == null) {
            return null;
        }
        if (h.data == data) {
            LLNode t = h.next;
            h.next = null;
            return t;
        }
        h.next = deleteNode(h.next, data);
        return h;
    }
}
