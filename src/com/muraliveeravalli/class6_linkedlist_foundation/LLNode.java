package com.muraliveeravalli.class6_linkedlist_foundation;

public class LLNode {
    public int data;
    public LLNode next;

    public LLNode(int d) {
        data = d;
        next = null;
    }

    public static void printList(LLNode head) {
        LLNode temp = head;
        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }

    @Override
    public String toString() {
        return data + " ";
    }
}
