package com.muraliveeravalli.class6_linkedlist_foundation;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertInBeginning.insertInBeginning;

public class ReturnLastNode {
    public static void main(String[] args) {
        LLNode llNode = insertInBeginning(null, 1);
        llNode = insertInBeginning(llNode, new int[]{2, 3, 4, 5});
        LLNode.printList(llNode);
        LLNode lastNode = getLastNode(llNode);
        System.out.println(lastNode);


    }

    static LLNode getLastNode(LLNode head) {
        if (head == null) {
            return null;
        }
        if (head.next == null) {
            return head;
        }
        return getLastNode(head.next);
    }
}
