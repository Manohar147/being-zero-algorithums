package com.muraliveeravalli.class6_linkedlist_foundation;

import java.util.Arrays;

public class InsertInBeginning {
    public static void main(String[] args) {
        LLNode llNode = insertInBeginning(null, 1);
        llNode = insertInBeginning(llNode, new int[]{2, 3, 4, 5});
        LLNode.printList(llNode);
    }

    static LLNode insertInBeginning(LLNode head, int... data) {
        for (int i = 0; i < data.length; i++) {
            head = insertInBeginning(head, data[i]);
        }
        return head;
    }

    static LLNode insertInBeginning(LLNode head, int data) {
        LLNode node = new LLNode(data);
        if (head == null) {
            return node;
        }
        node.next = head;
        return node;
    }
}
