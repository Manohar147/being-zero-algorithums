package com.muraliveeravalli.class7_linkedlist_interview_questions;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

public class DetectCycleInLinkedList {

    public static void main(String[] args) {

    }

    static boolean hasCycle(LLNode head) {
        if (head == null) {
            return false;
        }
        LLNode fast = head.next, slow = head;
        while (fast != null && fast.next != null) {
            if (fast.data == slow.data) {
                return true;
            }
            fast = fast.next.next;
            slow = slow.next;
        }
        return false;
    }
}
