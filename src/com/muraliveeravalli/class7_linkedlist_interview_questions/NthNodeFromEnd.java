package com.muraliveeravalli.class7_linkedlist_interview_questions;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class NthNodeFromEnd {

    public static void main(String[] args) {
        LLNode head = insertAtEnd(null, 1);
        head = insertAtEnd(head, new int[]{2, 3, 4, 5});
        LLNode.printList(head);
        LLNode nthNode = getNthFromEnd(head, 3);
        System.out.println(nthNode);
    }

    static LLNode getNthFromEnd(LLNode head, int n) {
        if (head == null) {
            return null;
        }
        LLNode t1 = head, t2 = head;
        for (int i = 0; i < n; i++) {
            if (t2 == null) {
                return null;
            }
            t2 = t2.next;
        }
        while (t2 != null) {
            t2 = t2.next;
            t1 = t1.next;
        }
        return t1;
    }
}
