package com.muraliveeravalli.class7_linkedlist_interview_questions;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

public class ReverseAGivenLinkedList {

    public static void main(String[] args) {

    }

    LLNode reverseList(LLNode head) {
        LLNode c = head, n = null, p = null;
        while (c != null) {
            n = c.next;
            c.next = p;
            p = c;
            c = n;
        }
        return p;
    }
}
