package com.muraliveeravalli.class7_linkedlist_interview_questions;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class FindMiddleNode {

    public static void main(String[] args) {
        LLNode head = insertAtEnd(null, 1);
        head = insertAtEnd(head, new int[]{2, 3});
        LLNode.printList(head);
        LLNode middle = getMiddleNode(head);
        System.out.println(middle);

    }

    static LLNode getMiddleNode(LLNode head) {
        if (head == null) {
            return null;
        }
        LLNode t1 = head, t2 = head;
        while (t1!= null && t1.next != null && t1.next.next != null) {
            t1 = t1.next.next;
            t2 = t2.next;
        }
        return t2;
    }
}
