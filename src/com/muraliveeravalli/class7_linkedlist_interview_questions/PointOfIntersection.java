package com.muraliveeravalli.class7_linkedlist_interview_questions;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

public class PointOfIntersection {

    public static void main(String[] args) {

    }

    LLNode pointOfIntersection(LLNode h1, LLNode h2) {
        LLNode c1 = h1, c2 = h2;
        int ca = 0, cb = 0;
        while (c1 != null) {
            ca++;
            c1 = c1.next;
        }
        while (c2 != null) {
            cb++;
            c2 = c2.next;
        }
        c1 = h1;
        c2 = h2;
        int diff = ca > cb ? ca - cb : cb - ca;
        for (int i = 0; i < diff; i++) {
            if (ca > cb) {
                c1 = c1.next;
            } else {
                c2 = c2.next;
            }
        }
        while (c1 != null && c2 != null) {
            if (c1 == c2) {
                return c1;
            }
            c1 = c1.next;
            c2 = c2.next;
        }
        return null;
    }
}
