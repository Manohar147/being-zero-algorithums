package com.muraliveeravalli.class5_interview_questions_strings_and_arrays;

import java.util.Arrays;

public class RemoveDuplicateCharsInGivenArray {

    public static void main(String[] args) {
        char[] c = {'a', 'p', 'p', 'l', 'e'};
        removeDupChars(c);
        System.out.println(Arrays.toString(c));
    }

    static int removeDupChars(char arr[]) {
        int[] count = new int[26];
        int c = 0;
        for (int i = 0; i < arr.length; i++) {
            count[arr[i] - 'a']++;
        }
        for (int i = 0; i < count.length; i++) {
            if (count[i] > 0) {
                c++;
            }
        }
        char[] mArr = new char[arr.length];
        int d = 0;
        for (int i = 0; i < arr.length; i++) {
            if (count[arr[i] - 'a'] > 0) {
                mArr[d] = arr[i];
                d++;
                count[arr[i] - 'a'] = 0;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i] = mArr[i];
        }
        return c;
    }
}
