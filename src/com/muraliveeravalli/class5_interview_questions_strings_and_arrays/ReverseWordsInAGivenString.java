package com.muraliveeravalli.class5_interview_questions_strings_and_arrays;

import java.util.Arrays;

public class ReverseWordsInAGivenString {

    public static void main(String[] args) {
        String s = "this is a string";
        char[] c1 = s.toCharArray();
        reverseWords(c1);
        System.out.println(Arrays.toString(c1));
    }

    static void reverse(char[] c, int start, int end) {
        if (start >= end) {
            return;
        }
        char t = c[start];
        c[start] = c[end];
        c[end] = t;
        reverse(c, start + 1, end - 1);
    }

    static void reverseWords(char arr[]) {
        reverse(arr, 0, arr.length - 1);
        int start = 0, end = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == ' ') {
                end = i - 1;
                reverse(arr, start, end);
                start = i + 1;
            }
            if (i == arr.length - 1) {
                reverse(arr, start, arr.length - 1);
            }
        }
    }
}
