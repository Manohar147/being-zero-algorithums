package com.muraliveeravalli.class5_interview_questions_strings_and_arrays;

public class HasUnqiueChars {

    public static void main(String[] args) {
        char[] s = {'t', 'h', 'i', 's'};
        System.out.println(hasUniqueChars(s));
    }

    static boolean hasUniqueChars(char arr[]) {
        int[] freq = new int[26];
        for (int i = 0; i < arr.length; i++) {
            freq[arr[i] - 'a']++;
        }
        for (int i = 0; i < freq.length; i++) {
            if (freq[i] > 1) {
                return false;
            }
        }
        return true;
    }
}
