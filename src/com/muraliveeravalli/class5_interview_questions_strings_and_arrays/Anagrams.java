package com.muraliveeravalli.class5_interview_questions_strings_and_arrays;

public class Anagrams {

    public static void main(String[] args) {
        System.out.println(areAnagrams("ABCD", "CDAB"));
        System.out.println(areAnagrams("ABCDE", "CDAB"));

    }

    static boolean areAnagrams(String first, String second) {
        first = first.toLowerCase();
        second = second.toLowerCase();
        int[] freq = new int[26];
        for (int i = 0; i < first.length(); i++) {
            freq[first.charAt(i) - 'a']++;
        }
        for (int i = 0; i < second.length(); i++) {
            freq[second.charAt(i) - 'a']--;
        }

        for (int i = 0; i < freq.length; i++) {
            if (freq[i] != 0) {
                return false;
            }
        }
        return true;

    }
}
