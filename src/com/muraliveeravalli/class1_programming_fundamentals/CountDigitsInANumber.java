package com.muraliveeravalli.class1_programming_fundamentals;

public class CountDigitsInANumber {

    public static void main(String[] args) {
        System.out.println(digitCount(123));
        System.out.println(digitCount(1));
    }

    static int digitCount(int n) {
        if (n == 0) {
            return 1;
        }
        int count = 0;
        while (n > 0) {
            n = n / 10;
            count++;
        }
        return count;
    }
}
