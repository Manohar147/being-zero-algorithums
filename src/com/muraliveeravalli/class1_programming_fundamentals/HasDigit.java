package com.muraliveeravalli.class1_programming_fundamentals;

public class HasDigit {

    public static void main(String[] args) {
        System.out.println(hasDigit(1234, 3));
        System.out.println(hasDigit(1234, 9));
    }

    static boolean hasDigit(int n, int d) {
        if (n == d) {
            return true;
        }
        while (n > 0) {
            if (n % 10 == d) {
                return true;
            }
            n /= 10;
        }
        return false;
    }
}
