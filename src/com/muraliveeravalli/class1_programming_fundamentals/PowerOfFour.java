package com.muraliveeravalli.class1_programming_fundamentals;

public class PowerOfFour {
    public static void main(String[] args) {
        System.out.println(isPowerOfFour(1024));
        System.out.println(isPowerOfFour(16));
        System.out.println(isPowerOfFour(8));
    }

    static boolean isPowerOfFour(int n) {
        if ((n & (n - 1)) != 0) {
            return false;
        }
        int count = 0;
        while (n > 1) {
            n = n >> 1;
            count++;
        }
        return count % 2 == 0;

    }
}
