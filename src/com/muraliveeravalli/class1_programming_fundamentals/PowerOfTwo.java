package com.muraliveeravalli.class1_programming_fundamentals;

public class PowerOfTwo {
    public static void main(String[] args) {
        System.out.println(isPowerOfTwo(1024));
        System.out.println(isPowerOfTwo(14));
    }

    static boolean isPowerOfTwo(int n) {
        return (n & (n - 1)) == 0;
    }
}
