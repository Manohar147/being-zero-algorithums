package com.muraliveeravalli.class4_two_dimensional_arrays;

public class PrintMatrixSpirally {

    public static void main(String[] args) {
        int array[][] = new int[4][4];
        int count = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = count++;
            }
        }

        printArray(array);
        System.out.println("---------------");
        printSpirally(array);

    }

    static void printArray(int arr[][]) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.print("\n");
        }

    }

    static void printSpirally(int arr[][]) {
        int row = -1, col = arr.length, bottom = arr.length, left = -1;
        int len = arr.length;
        String result = "";
        while (row <= col) {
            //go left
            row = row + 1;
            for (int i = row; i < col; i++) {
                result += arr[row][i] + ", ";
            }
            //go down
            col = col - 1;
            for (int i = row + 1; i < bottom; i++) {
                result += arr[i][col] + ", ";
            }
            //go left
            bottom = bottom - 1;
            for (int i = col - 1; i >= row; i--) {
                result += arr[bottom][i] + ", ";
            }
            //go Up
            left = left + 1;
            for (int i = bottom - 1; i > row; i--) {
                result += arr[i][left] + ", ";
            }
        }
        result = result.substring(0, result.length() - 2);
        System.out.print(result);

    }
}
