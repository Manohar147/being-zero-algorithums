package com.muraliveeravalli.class4_two_dimensional_arrays;

public class ValidateMagicSquare {

    public static void main(String[] args) {
        int arr[][] = {{8, 1, 6}, {3, 5, 7}, {4, 9, 2}};
        System.out.println(validateMagicSq(arr));
    }

    static boolean validateMagicSq(int arr[][]) {
        if (arr == null) {
            return false;
        }
        int diagonal = 0;
        int total = 0;
        int rowSum = 0;
        int colSum = 0;
        for (int i = 0; i < arr.length; i++) {
            total += arr[i][i];
        }
        for (int i = 0, j = arr.length - 1; i < arr.length; i++, j--) {
            diagonal += arr[i][j];
        }
        if (total != diagonal) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            rowSum = 0;
            colSum = 0;
            for (int j = 0; j < arr.length; j++) {
                rowSum += arr[i][j];
                colSum += arr[j][i];
            }
            if (rowSum != total || colSum != total) {
                return false;
            }
        }
        return true;
    }
}
