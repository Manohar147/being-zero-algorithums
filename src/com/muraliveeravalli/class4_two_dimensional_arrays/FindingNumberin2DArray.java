package com.muraliveeravalli.class4_two_dimensional_arrays;

public class FindingNumberin2DArray {

    public static void main(String[] args) {
        int arr[][] = {{1, 2, 3, 4},
                {6, 8, 9, 10},
                {7, 11, 12, 13}};

        findNumberInSortedGrid(arr, 6);
//        findNumberInSortedGrid(arr, 9);

    }

    static void findNumberInSortedGrid(int arr[][], int key) {
        int n = arr[0].length;
        int i = 0, j = n - 1;
        while (i < arr.length && j >= 0) {
            if (arr[i][j] == key) {
                System.out.print(i + " " + j + "\n");
                return;
            }
            if (arr[i][j] > key)
                j--;
            else
                i++;
        }
        System.out.print("-1\n");
    }
}
