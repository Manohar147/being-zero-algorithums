package com.muraliveeravalli.class4_two_dimensional_arrays;

public class ValidateASudoku {
    public static void main(String[] args) {

    }

    boolean validateSudoku(int arr[][]) {
        int rowSum = 0, colSum = 0, boxSum = 0, total = 45;
        int freq[] = new int[10];
        for (int i = 0; i < arr.length; i += 3) {
            for (int j = 0; j < arr.length; j += 3) {
                boxSum = 0;
                for (int k = i; k < i + 3; k++) {
                    for (int l = j; l < j + 3; l++) {
                        boxSum += arr[k][l];
                    }
                }
                if (boxSum != total) {
                    return false;
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {
            rowSum = 0;
            colSum = 0;
            for (int j = 0; j < arr.length; j++) {
                rowSum += arr[i][j];
                colSum += arr[j][i];
                if (arr[i][j] > 9 || arr[i][j] < 1) {
                    return false;
                }
                freq[arr[i][j]]++;
            }
            if (rowSum != total || colSum != total) {
                return false;
            }
        }
        for (int i = 1; i < freq.length; i++) {
            if (freq[i] != 9) {
                return false;
            }
        }

        return true;
    }
}
