package com.muraliveeravalli.class4_two_dimensional_arrays;

public class GenerateMagicSquare {

    public static void main(String[] args) {

    }

    void fillMagicSq(int ms[][]) {
        int i = 0;
        int j = ms.length / 2;
        int count = 1;
        int n = ms.length;
        int nextI = 0, nextJ = 0;
        while (count <= n * n) {
            ms[i][j] = count++;
            nextI = (i - 1) % n;
            if (nextI < 0) {
                nextI = n + nextI;
            }
            nextJ = (j + 1) % n;
            if (ms[nextI][nextJ] != 0) {
                nextI = (i + 1) % n;
                nextJ = j;
            }
            i = nextI;
            j = nextJ;
        }
    }

}
