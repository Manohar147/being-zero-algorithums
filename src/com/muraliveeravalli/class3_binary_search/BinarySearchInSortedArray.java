package com.muraliveeravalli.class3_binary_search;

public class BinarySearchInSortedArray {

    public static void main(String[] args) {
        int arr[] = {-8, -7, 2, 7};

        System.out.println(findingIdx(arr, 7));
        int arr1[] = {0, 12, 25};
        System.out.println(findingIdx(arr1, 11));


    }

    static int findingIdx(int arr[], int k) {
        int left = 0, right = arr.length - 1, mid = 0;
        while (left <= right) {
            mid = (left + right) / 2;
            if (arr[mid] == k) {
                return mid;
            } else if (k > arr[mid]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return -1;
    }
}
