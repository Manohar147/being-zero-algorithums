package com.muraliveeravalli.class3_binary_search;

/**
 * Find Index with A[i]==i
 */
public class FindIndexWithAOfIEqualToIi {

    public static void main(String[] args) {
        int arr[] = {-8, -7, 2, 7, 8};
        System.out.println(findingIdx(arr));
        int arr1[] = {0, 12, 25};
        System.out.println(findingIdx(arr1));

    }

    static int findingIdx(int arr[]) {
        int left = 0, right = arr.length - 1, mid = 0;
        while (left <= right) {
            mid = (left + right) / 2;
            if (arr[mid] == mid) {
                return mid;
            } else if (mid < arr[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return -1;
    }
}
