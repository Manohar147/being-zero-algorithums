package com.muraliveeravalli.class3_binary_search;

public class SearchElementInSortedRotatedArray {
    public static void main(String[] args) {
        int arr[] = {4, 5, 6, 1, 2, 3};
        System.out.println(getIdxInSortedRotatedArr(arr, 5));
        int arr1[] = {9, 10, 11, 12, 7, 8};
        System.out.println(getIdxInSortedRotatedArr(arr1, 6));
    }

    static int getIdxInSortedRotatedArr(int a[], int k) {
        int left = 0, right = a.length - 1, mid = 0;
        while (left <= right) {
            mid = (left + right) / 2;
            if (a[mid] == k) {
                return mid;
            } else if (k > a[mid]) {
                if (k > a[right]) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            } else {
                if (k < a[left]) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
        }
        return -1;
    }
}
