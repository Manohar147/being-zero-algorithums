package com.muraliveeravalli.class2_one_dimensional_arrays;

import java.util.Arrays;

public class LadiesFirst {

    public static void main(String[] args) {
        String a = "GLGLG";
        ladiesFirst(a.toCharArray());
        ladiesFirst("GGGLLL".toCharArray());

    }

    static void ladiesFirst(char arr[]) {
        int l = 0;
        int g = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 'l' || arr[i] == 'L') {
                l++;
            }
            if (arr[i] == 'g' || arr[i] == 'G') {
                g++;
            }
        }

        for (int i = 0; i < l; i++) {
            arr[i] = 'L';
        }
        for (int i = l; i < (l + g); i++) {
            arr[i] = 'G';
        }

        System.out.println(Arrays.toString(arr));

    }
}
