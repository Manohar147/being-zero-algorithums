package com.muraliveeravalli.class2_one_dimensional_arrays;

public class MinAndMaxArrayElement {

    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 5, 6, 2, 9};
        printMinAndMax(arr);
    }

    static void printMinAndMax(int arr[]) {
        int min = arr[0];
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.print(min + " " + max);
        // TODO:  Your code goes here.
    }
}
