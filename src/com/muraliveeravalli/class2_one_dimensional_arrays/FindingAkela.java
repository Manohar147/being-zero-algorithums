package com.muraliveeravalli.class2_one_dimensional_arrays;

public class FindingAkela {

    public static void main(String[] args) {
        int arr[] = {1, 2, 1, 3, 4, 4, 3};
        int arr1[] = {9, 9, 5, 6, 6};

        System.out.println(findAkela(arr));
        System.out.println(findAkela(arr1));
    }

    static int findAkela(int arr[]) {
        int c = arr[0];
        for (int i = 1; i < arr.length; i++) {
            c = c ^ arr[i];
        }
        return c;
    }
}
