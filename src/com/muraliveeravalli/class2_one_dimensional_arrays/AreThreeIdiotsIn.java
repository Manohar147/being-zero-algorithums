package com.muraliveeravalli.class2_one_dimensional_arrays;

public class AreThreeIdiotsIn {

    public static void main(String[] args) {
        int arr[] = {1, 4, 5, 6, 2};
        int arr1[] = {1, 2, 3};
        int arr2[] = {1, 2, 4};
        System.out.println(areThreeIdiotsIn(arr));
        System.out.println(areThreeIdiotsIn(arr1));
        System.out.println(areThreeIdiotsIn(arr2));
    }

    static boolean areThreeIdiotsIn(int arr[]) {
        if (arr.length < 2) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            if ((i + 2) < arr.length) {
                if ((arr[i] + arr[i + 2]) == arr[i + 1] * 2 && arr[i] < arr[i + 2]) {
                    return true;
                }
            }
        }
        return false;
    }
}
