package com.muraliveeravalli.class2_one_dimensional_arrays;

import java.util.Arrays;

public class ReverseAGivenString {
    public static void main(String[] args) {
        String a = "hello";
        reverse(a.toCharArray());
        String b = "this";
        reverse(b.toCharArray());
    }

    static void reverse(char arr[]) {
        for (int i = 0; i < arr.length / 2; i++) {
            char temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }
        System.out.println(Arrays.toString(arr));
    }
}
