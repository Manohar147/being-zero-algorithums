package com.muraliveeravalli.class2_one_dimensional_arrays;

public class PrintSecondHighestNumberOfArray {
    public static void main(String[] args) {
        int arr[] = {1, 2, 5};
        int arr1[] = {1, 2, 9, 20, 5};
        printSecondHighest(arr);
        printSecondHighest(arr1);

    }

    static void printSecondHighest(int a[]) {
        int max = a[0];
        int max2 = a[1];
        if (max2 > max) {
            max = max + max2;
            max2 = max - max2;
            max = max - max2;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max) {
                max2 = max;
                max = a[i];
            }
        }
        System.out.println(max2);
    }
}
