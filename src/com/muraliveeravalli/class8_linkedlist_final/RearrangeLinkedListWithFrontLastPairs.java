package com.muraliveeravalli.class8_linkedlist_final;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class RearrangeLinkedListWithFrontLastPairs {

    public static void main(String[] args) {
        LLNode head = insertAtEnd(null, 1);
        head = insertAtEnd(head, new int[]{2, 3, 4, 5});
        LLNode.printList(head);
        frontLastRearrange(head);
        LLNode.printList(head);

    }

    static void frontLastRearrange(LLNode head) {
        if (head == null) {
            return;
        }
        LLNode m = middle(head), reverse = reverse(m.next);
        m.next = null;
        LLNode n = new LLNode(-1), temp = n;
        while (head != null || reverse != null) {
            if (head != null) {
                n.next = head;
                n = n.next;
                head = head.next;
            }
            if (reverse != null) {
                n.next = reverse;
                n = n.next;
                reverse = reverse.next;
            }
        }
        head = temp.next;
    }

    static LLNode reverse(LLNode head) {
        LLNode c = head, n = null, p = null;
        while (c != null) {
            n = c.next;
            c.next = p;
            p = c;
            c = n;
        }
        return p;
    }

    public static LLNode middle(LLNode head) {
        if (head == null) {
            return null;
        }
        LLNode temp = head;
        while (head != null && head.next != null && head.next.next != null) {
            head = head.next.next;
            temp = temp.next;
        }
        return temp;
    }
}
