package com.muraliveeravalli.class8_linkedlist_final;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

public class SortedInsertInGivenLinkedList {

    public static void main(String[] args) {

        LLNode head = sortedInsert(null, 6);
        head = sortedInsert(head, 5);
        head = sortedInsert(head, 1);
        head = sortedInsert(head, 2);
        head = sortedInsert(head, 3);
        head = sortedInsert(head, 4);

        LLNode.printList(head);

    }

    static LLNode sortedInsert(LLNode head, int data) {
        if (head == null) {
            return new LLNode(data);
        }
        if (head.data > data) {
            LLNode node = new LLNode(data);
            node.next = head;
            return node;
        }
        head.next = sortedInsert(head.next, data);
        return head;
    }
}
