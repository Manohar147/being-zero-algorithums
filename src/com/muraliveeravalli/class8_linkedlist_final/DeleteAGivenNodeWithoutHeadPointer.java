package com.muraliveeravalli.class8_linkedlist_final;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

public class DeleteAGivenNodeWithoutHeadPointer {

    public static void main(String[] args) {

    }


    void deleteNodeWithoutHead(LLNode nodeToDelete) {
        LLNode c = nodeToDelete;
        LLNode n = c.next;
        c.data = n.data;
        c.next = n.next;
        n.next = null;
    }
}
