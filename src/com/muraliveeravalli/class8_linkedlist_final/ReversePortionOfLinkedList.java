package com.muraliveeravalli.class8_linkedlist_final;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class ReversePortionOfLinkedList {

    public static void main(String[] args) {

        LLNode head = insertAtEnd(null, 10);
        head = insertAtEnd(head, new int[]{20, 30, 40, 50}); //10 -> 40 -> 30 -> 20 -> 50
        LLNode.printList(head);
        head = reversePartial(head, 2, 4);
        LLNode.printList(head);
    }

    static LLNode reversePartial(LLNode head, int S, int E) {
        int count = count(head);
        LLNode first = null, end = null, middle = null, temp = head, temp1 = head, next = head;
        if (S == 1 && E == count(head)) {
            return reverse(head);
        }
        int c1 = 1, c2 = 0;
        if (S == 1) {
            first = null;
        } else {
            while (c1 < (S - 1) && temp != null) {
                temp = temp.next;
                c1++;
            }
            next = temp.next;
            temp.next = null;
            first = head;
        }
        c2 = c1;
        if (E == count) {
            end = null;
        } else {
            if (S == 1) {
                c2 = 0;
            }
            temp = next;
            while (c2 < (E - 1)) {
                temp = temp.next;
                c2++;
            }
            end = temp.next;
            temp.next = null;
        }
        LLNode finalResult = null;
        LLNode rev = reverse(next);
        if (first != null) {
            LLNode firstDummy = first;
            while (first.next != null) {
                first = first.next;
            }
            first.next = rev;
            finalResult = firstDummy;
        } else {
            finalResult = rev;
        }

        if (end == null) {
            return finalResult;
        } else {
            LLNode finalDummy = finalResult;
            while (finalResult.next != null) {
                finalResult = finalResult.next;
            }
            finalResult.next = end;
            return finalDummy;
        }

    }

    static int count(LLNode head) {
        LLNode temp = head;
        int n = 0;
        while (temp != null) {
            n++;
            temp = temp.next;
        }
        return n;
    }


    static LLNode reverse(LLNode head) {
        LLNode p = null, c = head, n;
        while (c != null) {
            n = c.next;
            c.next = p;
            p = c;
            c = n;
        }
        return p;
    }
}
