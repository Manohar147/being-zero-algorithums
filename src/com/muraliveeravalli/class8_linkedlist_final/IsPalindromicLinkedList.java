package com.muraliveeravalli.class8_linkedlist_final;

import com.muraliveeravalli.class6_linkedlist_foundation.LLNode;

import static com.muraliveeravalli.class6_linkedlist_foundation.InsertAtEnd.insertAtEnd;

public class IsPalindromicLinkedList {

    public static void main(String[] args) {

        LLNode head = insertAtEnd(null, 1);
        LLNode.printList(head);
        boolean ans = isPalindromicList(head);
        System.out.println(ans);

    }

    static boolean isPalindromicList(LLNode A) {
        if (A == null) {
            return true;
        }
        LLNode temp = A, middle = null, reverse = null;
        int c = 0;
        while (temp != null) {
            temp = temp.next;
            c++;
        }
        middle = findMiddle(A);
        reverse = reverse(middle.next);
        middle.next = null;
        for (int i = 0; i < c / 2; i++) {
            if (A.data != reverse.data) {
                return false;
            }
            A = A.next;
            reverse = reverse.next;
        }
        return true;
    }


    static LLNode findMiddle(LLNode head) {
        LLNode fast = head, slow = head;
        while (fast != null && fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    static LLNode reverse(LLNode head) {
        LLNode c = head, p = null, n;

        while (c != null) {
            n = c.next;
            c.next = p;
            p = c;
            c = n;
        }
        return p;
    }
}
